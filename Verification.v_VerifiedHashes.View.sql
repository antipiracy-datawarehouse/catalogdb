USE [Catalog]
GO
/****** Object:  View [Verification].[v_VerifiedHashes]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [Verification].[v_VerifiedHashes]
WITH SCHEMABINDING
as
select 
  F.Sha1Hash as Hashtext
  ,10 as NetworkId  
  ,'BitTorrent' as ProtocolName
  ,f.FileSize
  ,f.GidReferenceId
  ,f.TitleId   
  ,f.FileFormatId
  ,f.ContentQualityId
  ,f.VerificationTypeId               
from Verification.[File] as f 
  where FileHashStatusId in (20,22) 
union 
select 
  F.Sha1Hash as Hashtext
  ,30 as NetworkId  
  ,'Gnutella' as ProtocolName
  ,f.FileSize
  ,f.GidReferenceId
  ,f.TitleId   
  ,f.FileFormatId
  ,f.ContentQualityId
  ,f.VerificationTypeId               
from Verification.[File] as f 
  where FileHashStatusId in (20,22) 
union 
select 
  F.Sha1Hash as Hashtext
  ,40 as NetworkId  
  ,'Ares' as ProtocolName
  ,f.FileSize
  ,f.GidReferenceId
  ,f.TitleId   
  ,f.FileFormatId
  ,f.ContentQualityId
  ,f.VerificationTypeId               
from Verification.[File] as f 
  where FileHashStatusId in (20,22)   
union 
select 
  F.Md4Hash as Hashtext
  ,20 as NetworkId  
  ,'eDonkey' as ProtocolName
  ,f.FileSize
  ,f.GidReferenceId
  ,f.TitleId   
  ,f.FileFormatId
  ,f.ContentQualityId
  ,f.VerificationTypeId               
from Verification.[File] as f 
  where FileHashStatusId in (20,22) and f.Md4Hash is not null  
union 
select 
  F.Sha1Hash32 as Hashtext
  ,30 as NetworkId  
  ,'Gnutella' as ProtocolName
  ,f.FileSize
  ,f.GidReferenceId
  ,f.TitleId   
  ,f.FileFormatId
  ,f.ContentQualityId
  ,f.VerificationTypeId               
from Verification.[File] as f 
  where FileHashStatusId in (20,22) and f.Sha1Hash32 is not null    
union 
select 
  F.Md5Hash as Hashtext
  ,50 as NetworkId  
  ,'DirectConnect' as ProtocolName
  ,f.FileSize
  ,f.GidReferenceId
  ,f.TitleId   
  ,f.FileFormatId
  ,f.ContentQualityId
  ,f.VerificationTypeId               
from Verification.[File] as f 
  where FileHashStatusId in (20,22) and f.Md5Hash is not null
GO
