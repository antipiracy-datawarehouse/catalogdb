USE [Catalog]
GO
/****** Object:  Table [Stage].[StreamHash]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[StreamHash](
	[Id] [int] NOT NULL,
	[HashText] [varchar](75) NOT NULL,
	[FlvPath] [varchar](40) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
