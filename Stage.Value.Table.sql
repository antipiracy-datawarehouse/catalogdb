USE [Catalog]
GO
/****** Object:  Table [Stage].[Value]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[Value](
	[Id] [bigint] NOT NULL,
	[TitleNodeId] [int] NOT NULL,
	[DefinitionId] [smallint] NOT NULL,
	[ValueTypeId] [tinyint] NOT NULL,
	[IntegerKey] [bigint] NULL,
	[EnumKey] [int] NULL,
	[StringKey] [nvarchar](1024) NULL,
	[IntegerValue] [bigint] NULL,
	[EnumValue] [int] NULL,
	[FloatValue] [real] NULL,
	[DateValue] [date] NULL,
	[DateTimeValue] [datetime2](2) NULL,
	[StringValue] [nvarchar](2048) NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL
) ON [Stage_FG]
GO
