USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[f_BinaryHashToText]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[f_BinaryHashToText](@ProtocolGid [tinyint], @Hash [varbinary](8000))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[UserDefinedFunctions].[f_BinaryHashToText]
GO
