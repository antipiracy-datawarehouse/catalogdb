USE [Catalog]
GO
/****** Object:  StoredProcedure [Metadata].[usp_GetTitleMetadata]    Script Date: 11/24/2020 09:45:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [Metadata].[usp_GetTitleMetadata]
	@titleIds [dbo].[ListOfNumbers] READONLY,
	@definitionIds [dbo].[ListOfNumbers] READONLY
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		IF (OBJECT_ID('tempdb..#titleIds') IS NOT NULL) DROP TABLE #titleIds;
		WITH [cte] AS
		(
			SELECT
				[@ti].[Number] AS [RequestedTitleId],
				[t].[Id] AS [TitleId],
				[t].[TitleTypeId],
				[t].[ParentTitleId]
			FROM
				@titleIds AS [@ti]
				INNER JOIN [Title].[Title] AS [t]
					ON [@ti].[Number] = [t].[Id]

			UNION ALL

			SELECT
				[cte].[RequestedTitleId],
				[t].[Id] AS [TitleId],
				[t].[TitleTypeId],
				[t].[ParentTitleId]
			FROM
				[Title].[Title] AS [t]
				INNER JOIN [cte]
					ON [cte].[ParentTitleId] = [t].[Id]
		)
		SELECT DISTINCT
			CAST([cte].[RequestedTitleId] AS INT) AS [TitleId],
			[cte].[TitleId] AS [TitleNodeIds],
			[tt].[Level]
		INTO
			#titleIds
		FROM
			[cte]
			INNER JOIN [Title].[TitleType] AS [tt]
				ON [tt].[Id] = [cte].[TitleTypeId];
	
		IF (OBJECT_ID('tempdb..#metadataValues') IS NOT NULL) DROP TABLE #metadataValues;
		SELECT
			[#ti].[TitleId],
			[#ti].[Level],
			[v].[DefinitionId],
			[d].[DerivationTypeId],
			CAST(HASHBYTES
			(
				'MD5',
				LOWER
				(
					(
						CASE
							WHEN [d].[ValueTypeId] IN (3, 4) THEN 
								ISNULL(CONVERT(NVARCHAR(255), [v].[IntegerKey]), '')
								+ ISNULL(CONVERT(NVARCHAR(255), [v].[EnumKey]), '')
								+ ISNULL([v].[StringKey], '')
							ELSE ''
						END
					) + (
						CASE
							WHEN [d].[ValueTypeId] IN (2, 4) THEN 
								ISNULL(CONVERT(NVARCHAR(255), [v].[IntegerValue]), '')
								+ ISNULL(CONVERT(NVARCHAR(255), [v].[EnumValue]), '')
								+ ISNULL(CONVERT(NVARCHAR(255), [v].[FloatValue]), '')
								+ ISNULL(CONVERT(NVARCHAR(255), [v].[DateValue], 126), '')
								+ ISNULL(CONVERT(NVARCHAR(255), [v].[DateTimeValue], 126), '')
								+ ISNULL([v].[StringValue], '')
							ELSE ''
						END
					)
				)
			)
			AS BINARY(16)) AS [UniquenessContext],
			[v].[IntegerKey],
			[ei_key].[Name] AS [EnumKey],
			[v].[StringKey],
			[v].[IntegerValue],
			[ei_value].[Name] AS [EnumValue],
			[v].[FloatValue],
			[v].[DateValue],
			[v].[DateTimeValue],
			[v].[StringValue]
		INTO
			#metadataValues
		FROM
			[Metadata].[Value] AS [v]
			INNER JOIN #titleIds AS [#ti]
				ON [#ti].[TitleNodeIds] = [v].[TitleNodeId]
			INNER JOIN @definitionIds AS [@di]
				ON [@di].[Number] = [v].[DefinitionId]
			INNER JOIN [Metadata].[Definition] AS [d]
				ON [d].[Id] = [@di].[Number]
			LEFT JOIN [Metadata].[EnumItem] AS [ei_key]
				ON [ei_key].[Id] = [v].[EnumKey]
			LEFT JOIN [Metadata].[EnumItem] AS [ei_value]
				ON [ei_value].[Id] = [v].[EnumValue]
		WHERE
			[v].[IsDeleted] = 0
			AND
			(
				(
					[d].[DerivationTypeId] = 1
					AND [#ti].[TitleId] = [#ti].[TitleNodeIds]
				)
				OR [d].[DerivationTypeId] IN (2, 3)
			);

		SELECT
			[r].[TitleId],
			[r].[DefinitionId],
			[r].[IntegerKey],
			[r].[EnumKey],
			[r].[StringKey],
			[r].[IntegerValue],
			[r].[EnumValue],
			[r].[FloatValue],
			[r].[DateValue],
			[r].[DateTimeValue],
			[r].[StringValue]
		FROM
			(
				SELECT
					[#mv].[TitleId],
					[#mv].[DefinitionId],
					[#mv].[DerivationTypeId],
					[#mv].[IntegerKey],
					[#mv].[EnumKey],
					[#mv].[StringKey],
					[#mv].[IntegerValue],
					[#mv].[EnumValue],
					[#mv].[FloatValue],
					[#mv].[DateValue],
					[#mv].[DateTimeValue],
					[#mv].[StringValue],
					ROW_NUMBER() OVER
					(
						PARTITION BY
							[#mv].[TitleId],
							[#mv].[DefinitionId],
							[#mv].[UniquenessContext]
						ORDER BY
							[#mv].[Level] DESC
					)
					AS [UniquenessContextRowNumber],
					DENSE_RANK() OVER
					(
						PARTITION BY
							[#mv].[TitleId],
							[#mv].[DefinitionId]
						ORDER BY
							[#mv].[Level] DESC
					)
					AS [LevelDenseRank]
				FROM
					#metadataValues AS [#mv]
			) AS [r]
		WHERE
			[r].[UniquenessContextRowNumber] = 1
			AND
			(
				(
					[r].[DerivationTypeId] = 2
					AND [LevelDenseRank] = 1
				)
				OR [r].[DerivationTypeId] IN (1, 3)
			);
	END TRY
	BEGIN CATCH
		DECLARE @errorMessage NVARCHAR(2048) = (SELECT ERROR_MESSAGE());
		DECLARE @errorSeverity INT = (SELECT ERROR_SEVERITY());
		DECLARE @errorState INT = (SELECT ERROR_STATE());

		RAISERROR(@errorMessage, @errorSeverity, @errorState);

		RETURN -1;
	END CATCH
END
GO
