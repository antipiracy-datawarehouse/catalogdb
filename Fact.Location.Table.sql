USE [Catalog]
GO
/****** Object:  Table [Fact].[Location]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Fact].[Location](
	[Id] [int] NOT NULL,
	[CountryId] [smallint] NOT NULL,
	[Region] [varchar](50) NOT NULL,
	[State] [varchar](80) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Latitude] [decimal](7, 4) NULL,
	[Longitude] [decimal](7, 4) NULL,
	[DisplayLocationId] [int] NULL,
	[RegionOriginal] [varchar](50) NOT NULL,
	[StateOriginal] [varchar](80) NOT NULL,
	[CityOriginal] [varchar](50) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, DATA_COMPRESSION = PAGE) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
