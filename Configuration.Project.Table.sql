USE [Catalog]
GO
/****** Object:  Table [Configuration].[Project]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Configuration].[Project](
	[Id] [smallint] NOT NULL,
	[ProjectTypeId] [tinyint] NOT NULL,
	[ClientId] [smallint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[ProjectStart] [date] NULL,
	[ProjectEnd] [date] NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL,
 CONSTRAINT [pk_Configuration_Project] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [ix_Configuration_Project_ModifiedAt] ON [Configuration].[Project] 
(
	[ModifiedAt] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ux_Configuration_Project_Name] ON [Configuration].[Project] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
