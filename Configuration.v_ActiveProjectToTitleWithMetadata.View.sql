USE [Catalog]
GO
/****** Object:  View [Configuration].[v_ActiveProjectToTitleWithMetadata]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Configuration].[v_ActiveProjectToTitleWithMetadata]
AS
SELECT DISTINCT
[aptt].[ProjectId],
[mv].*
FROM
[Configuration].[v_ActiveProjectToTitle] AS [aptt]
INNER JOIN [Metadata].[Value] AS [mv]
ON [mv].[TitleNodeId] = [aptt].[TitleId]
WHERE
[mv].[IsDeleted] = 0;
GO
