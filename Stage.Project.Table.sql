USE [Catalog]
GO
/****** Object:  Table [Stage].[Project]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Project](
	[Id] [smallint] NOT NULL,
	[ProjectTypeId] [tinyint] NOT NULL,
	[CustomerId] [smallint] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Descr] [varchar](256) NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
