USE [Catalog]
GO
/****** Object:  View [Db].[V_IspPopularity2]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Db].[V_IspPopularity]'))
DROP VIEW [Db].[V_IspPopularity];
*/

CREATE view [Db].[V_IspPopularity2] as
select T.[IspId]
      ,isp.Name as [Isp]
      ,T.CountryId
      ,c.Name as Country
      ,T.[PopularityLastRank]
      ,T.[PopularityLastRank_Ns]
      ,T.[PopularityLastRank_L1]
from
(select [IspId], CountryId, 
         sum([PopularityLastRank_Ns]+[PopularityLastRank_L1]) [PopularityLastRank], 
         sum([PopularityLastRank_Ns]) [PopularityLastRank_Ns],
         sum([PopularityLastRank_L1]) [PopularityLastRank_L1]
 from (
	select ProjectTypeId, [IspId], CountryId
	      ,case when ProjectTypeId=11 then sum([PopularityLastRank]) else 0 end as [PopularityLastRank_Ns]
         ,case when ProjectTypeId<>11 then sum([PopularityLastRank]) else 0 end as [PopularityLastRank_L1]
					  FROM [Catalog].[Db].[IspPopularity] p
					  inner join Db.CustomerDatabase db on db.id=p.CustomerDatabaseId 
					  where [PopularityLastRank]>0  and p.[ModifiedAt]>=dateadd(day,-10,getutcdate())
					  group by ProjectTypeId, [IspId], CountryId
					  ) as T1
	group by [IspId], CountryId
) as T				  
inner join Fact.Isp as isp	on T.[IspId]=isp.[Id]
inner join Fact.Country as c on T.CountryId=c.Id;
GO
