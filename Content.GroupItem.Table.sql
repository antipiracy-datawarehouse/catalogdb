USE [Catalog]
GO
/****** Object:  Table [Content].[GroupItem]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Content].[GroupItem](
	[Id] [int] NOT NULL,
	[GroupDefinitionId] [tinyint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_GroupItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
