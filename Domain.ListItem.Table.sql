USE [Catalog]
GO
/****** Object:  Table [Domain].[ListItem]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Domain].[ListItem](
	[Id] [int] NOT NULL,
	[ListDefinitionId] [smallint] NOT NULL,
	[DomainId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ListItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_ListDefinitionId_DomainId] ON [Domain].[ListItem] 
(
	[ListDefinitionId] ASC,
	[DomainId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
ALTER TABLE [Domain].[ListItem] ADD  CONSTRAINT [DF_Domain_ListItem_Deleted]  DEFAULT ((0)) FOR [Deleted]
GO
