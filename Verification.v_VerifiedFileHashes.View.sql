USE [Catalog]
GO
/****** Object:  View [Verification].[v_VerifiedFileHashes]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [Verification].[v_VerifiedFileHashes] as
SELECT FH.id
 ,FH.HashText, FH.NetworkId, FH.FileSize -- unique key (it should be used to relate filehash)
 ,case FH.NetworkId when 10 then 'BitTorrent'
                    when 20 then 'eDonkey'
                    when 30 then 'Gnutella'
                    when 40 then 'Ares'
                    when 50 then 'DirectConnect' end as ProtocolName
,fh.GidReferenceId, fh.TitleId
,isnull(gr.GidTypeId, t.GidTypeId) as GidTypeId
,isnull(gr.ContentName,t.TitleName) as ContentName
,case when  fh.GidReferenceId is not null then gr.ContentItemName1 else TitleItemName1 end as ContentName1
,case when  fh.GidReferenceId is not null then gr.ContentItemName2 else TitleItemName2 end as ContentName2
,case when  fh.GidReferenceId is not null then gr.ContentItemName3 else TitleItemName3 end as ContentName3
,fh.VerifiedAt
,fh.FileFormatId
,fh.ContentQualityId
,FH.FileId
,f.VerificationTypeId
  FROM [Verification].[FileHash] FH
  left JOIN Content.GidReference GR ON GR.Id=Fh.GidReferenceId
  left join Fact.Title t on t.id=FH.TitleId  
  left join Verification.[File] as f on f.id=FH.FileId
  where fh.FileHashStatusId in (20,22)
GO
