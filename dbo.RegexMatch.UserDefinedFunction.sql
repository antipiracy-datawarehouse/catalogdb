USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[RegexMatch]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[RegexMatch](@input [nvarchar](max), @pattern [nvarchar](max))
RETURNS [bit] WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[RegExFunctions].[RegexMatch]
GO
