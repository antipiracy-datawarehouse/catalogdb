USE [Catalog]
GO
/****** Object:  Table [Stage].[GroupItemMember]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[GroupItemMember](
	[Id] [int] NOT NULL,
	[GroupItemId] [int] NOT NULL,
	[GidReferenceId] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[CustomerId] [smallint] NULL
) ON [Stage_FG]
GO
