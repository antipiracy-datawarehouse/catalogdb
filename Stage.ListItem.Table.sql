USE [Catalog]
GO
/****** Object:  Table [Stage].[ListItem]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[ListItem](
	[Id] [int] NOT NULL,
	[ListDefinitionId] [smallint] NOT NULL,
	[DomainId] [int] NOT NULL,
	[Deleted] [bit] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
