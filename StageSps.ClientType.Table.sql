USE [Catalog]
GO
/****** Object:  Table [StageSps].[ClientType]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [StageSps].[ClientType](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Description] [varchar](255) NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
