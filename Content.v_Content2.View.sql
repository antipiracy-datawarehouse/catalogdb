USE [Catalog]
GO
/****** Object:  View [Content].[v_Content2]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Content].[v_Content2]
AS
SELECT GR.Id as GidReferenceId
	, GR.Gid
	, GR.GidTypeId
	, gt.name as GidTypeName
	, gt.GidTypeGroupId
	, gtg.name as GidTypeGroupName 
	, GR.ContentName as [Name]
	, GR.ContentName2 as [Name2]
	,GR.ContentItemId1  
	,GR.ContentItemName1
    ,CONVERT([bigint],hashbytes('SHA1',isnull(GR.ContentItemName2,'N/A'))) ContentItemId2
    ,isnull(GR.ContentItemName2,'N/A') as ContentItemName2	
    ,CONVERT([bigint],hashbytes('SHA1',isnull(GR.ContentItemName3,'N/A'))) ContentItemId3
    ,isnull(GR.ContentItemName3,'N/A') as ContentItemName3	
	,cast(isNull(gr.CompanyId+10000, 0) as smallint) as CompanyId 
--	,co.name as CompanyName  
	,isNull(co.name, 'N/A') as CompanyName
FROM [Content].GidReference AS GR
inner join [Content].GidType gt on gt.id=gr.GidTypeId
inner join Content.GidTypeGroup gtg on gtg.id=gt.GidTypeGroupId
left join  Content.Company	co on co.id=gr.CompanyId
where GR.IsDeleted!=1
GO
