USE [Catalog]
GO
/****** Object:  View [Content].[v_Content]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Content].[v_Content]
AS
SELECT GR.Id as GidReferenceId
	, GR.Gid
	, GR.GidTypeId
	, gt.name as GidTypeName
	, GR.ContentName as [Name]
	, GR.ContentName2 as [Name2]
	,cast(GR.ContentItemName1 as varchar(150)) AS ContentItemName1 
	,cast(GR.ContentItemName2 as varchar(150)) AS ContentItemName2
	,cast(GR.ContentItemName3 as varchar(150)) AS ContentItemName3    
FROM [Content].GidReference AS GR
inner join [Content].GidType gt on gt.id=gr.GidTypeId
GO
