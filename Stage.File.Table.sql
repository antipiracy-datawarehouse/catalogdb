USE [Catalog]
GO
/****** Object:  Table [Stage].[File]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[File](
	[Id] [int] NOT NULL,
	[Sha1Hash] [char](40) NOT NULL,
	[Sha1Hash32] [char](32) NULL,
	[Md4Hash] [char](32) NULL,
	[Md5Hash] [char](32) NULL,
	[FileSize] [bigint] NOT NULL,
	[FirstFileName] [nvarchar](256) NOT NULL,
	[FileFormatId] [smallint] NOT NULL,
	[TitleId] [int] NULL,
	[GidReferenceId] [int] NULL,
	[ContentQualityId] [smallint] NOT NULL,
	[FileHashStatusId] [tinyint] NOT NULL,
	[VerificationTypeId] [tinyint] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[IsUploaded4Stream] [bit] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
