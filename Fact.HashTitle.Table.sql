USE [Catalog]
GO
/****** Object:  Table [Fact].[HashTitle]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Fact].[HashTitle](
	[Id] [int] NOT NULL,
	[FileHashId] [int] NOT NULL,
	[TitleId] [int] NULL,
	[GidReferenceId] [int] NULL,
	[Filename] [nvarchar](256) NOT NULL,
	[FirstSeenAt] [smalldatetime] NOT NULL,
	[HashTitleGid] [varbinary](16) NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_HashTitle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, DATA_COMPRESSION = PAGE) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_HashTitleGid] ON [Fact].[HashTitle] 
(
	[HashTitleGid] ASC
)
WHERE ([HashTitleGid] IS NOT NULL)
WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [Index_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_FileHashId_TitleId_GidReferenceId] ON [Fact].[HashTitle] 
(
	[FileHashId] ASC,
	[TitleId] ASC,
	[GidReferenceId] ASC
)
INCLUDE ( [Id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [Index_FG]
GO
