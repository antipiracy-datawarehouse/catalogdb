USE [Catalog]
GO
/****** Object:  Table [Stage].[MetaDataValue]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[MetaDataValue](
	[ContentItemId] [int] NOT NULL,
	[MetaDataDefinitionId] [int] NOT NULL,
	[Value] [nvarchar](256) NOT NULL
) ON [Stage_FG]
GO
