USE [Catalog]
GO
/****** Object:  Table [Stage].[Enum]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Enum](
	[Id] [smallint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Description] [varchar](256) NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
