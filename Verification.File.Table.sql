USE [Catalog]
GO
/****** Object:  Table [Verification].[File]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Verification].[File](
	[Id] [int] NOT NULL,
	[Sha1Hash] [char](40) NOT NULL,
	[Sha1Hash32] [char](32) NULL,
	[Md4Hash] [char](32) NULL,
	[Md5Hash] [char](32) NULL,
	[FileSize] [bigint] NOT NULL,
	[FirstFileName] [nvarchar](256) NOT NULL,
	[FileFormatId] [smallint] NOT NULL,
	[TitleId] [int] NULL,
	[GidReferenceId] [int] NULL,
	[ContentQualityId] [smallint] NOT NULL,
	[FileHashStatusId] [tinyint] NOT NULL,
	[VerificationTypeId] [tinyint] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[IsUploaded4Stream] [bit] NOT NULL,
 CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, DATA_COMPRESSION = PAGE) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_VerificationTypeId] ON [Verification].[File] 
(
	[VerificationTypeId] ASC
)
INCLUDE ( [Id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
