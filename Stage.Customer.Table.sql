USE [Catalog]
GO
/****** Object:  Table [Stage].[Customer]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Customer](
	[Id] [smallint] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[Descr] [varchar](256) NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
