USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[f_TextToIpAddress]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[f_TextToIpAddress](@value [nvarchar](max))
RETURNS [varbinary](8000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[UserDefinedFunctions].[f_TextToIpAddress]
GO
