USE [Catalog]
GO
/****** Object:  Table [StageSps].[Project]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [StageSps].[Project](
	[Id] [int] NOT NULL,
	[ProjectTypeId] [tinyint] NOT NULL,
	[ClientId] [smallint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[ProjectStart] [date] NULL,
	[ProjectEnd] [date] NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
