USE [Catalog]
GO
/****** Object:  View [Db].[V_IspPopularity]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Db].[V_IspPopularity]'))
DROP VIEW [Db].[V_IspPopularity];
*/

create view [Db].[V_IspPopularity] as
select T.[IspId]
      ,isp.Name as [Isp]
      ,T.CountryId
      ,c.Name as Country
      ,T.[PopularityLastRank]
from
(
select [IspId], CountryId, sum([PopularityLastRank]) as [PopularityLastRank]
				  FROM [Catalog].[Db].[IspPopularity]
				  where [PopularityLastRank]>0
				  group by [IspId], CountryId
				  ) as T
inner join Fact.Isp as isp	on T.[IspId]=isp.[Id]
inner join Fact.Country as c on T.CountryId=c.Id;
GO
