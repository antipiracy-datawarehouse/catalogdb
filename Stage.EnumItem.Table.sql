USE [Catalog]
GO
/****** Object:  Table [Stage].[EnumItem]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[EnumItem](
	[Id] [int] NOT NULL,
	[EnumId] [smallint] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[JsonPropertyName] [nvarchar](255) NOT NULL,
	[DisplayName] [nvarchar](255) NOT NULL,
	[Value] [int] NOT NULL,
	[IsDisabled] [bit] NOT NULL
) ON [Stage_FG]
GO
