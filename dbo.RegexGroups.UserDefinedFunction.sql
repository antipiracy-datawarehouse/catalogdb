USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[RegexGroups]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[RegexGroups](@input [nvarchar](max), @pattern [nvarchar](max))
RETURNS  TABLE (
	[Index] [int] NULL,
	[Group] [nvarchar](max) NULL,
	[Text] [nvarchar](max) NULL
) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[RegExFunctions].[RegexGroups]
GO
