USE [Catalog]
GO
/****** Object:  Table [Stage].[HashTitle]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[HashTitle](
	[Id] [int] NOT NULL,
	[FileHashId] [int] NOT NULL,
	[TitleId] [int] NULL,
	[GidReferenceId] [int] NULL,
	[Filename] [nvarchar](256) NOT NULL,
	[FirstSeenAt] [smalldatetime] NOT NULL,
	[HashTitleGid] [varbinary](16) NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
