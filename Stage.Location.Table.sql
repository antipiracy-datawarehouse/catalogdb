USE [Catalog]
GO
/****** Object:  Table [Stage].[Location]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Location](
	[Id] [int] NOT NULL,
	[CountryId] [smallint] NOT NULL,
	[Region] [varchar](50) NOT NULL,
	[State] [varchar](80) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[Latitude] [decimal](7, 4) NULL,
	[Longitude] [decimal](7, 4) NULL,
	[DisplayLocationId] [int] NULL,
	[RegionOriginal] [varchar](50) NOT NULL,
	[StateOriginal] [varchar](80) NOT NULL,
	[CityOriginal] [varchar](50) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
