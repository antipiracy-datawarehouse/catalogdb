USE [Catalog]
GO
/****** Object:  Table [Db].[IspPopularity]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Db].[IspPopularity](
	[CustomerDatabaseId] [smallint] NOT NULL,
	[IspId] [int] NOT NULL,
	[CountryId] [smallint] NOT NULL,
	[PopularityLastRank] [int] NOT NULL,
	[PopularityTopRank] [int] NOT NULL,
	[PopularityTopRankAt] [date] NULL,
	[ReportedTotalCount] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_IspPopularity] PRIMARY KEY CLUSTERED 
(
	[CustomerDatabaseId] ASC,
	[IspId] ASC,
	[CountryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, DATA_COMPRESSION = PAGE) ON [Data_FG]
) ON [Data_FG]
GO
CREATE NONCLUSTERED INDEX [IX_IspId_CountryId] ON [Db].[IspPopularity] 
(
	[IspId] ASC,
	[CountryId] ASC
)
INCLUDE ( [PopularityLastRank],
[PopularityTopRank],
[ReportedTotalCount]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, DATA_COMPRESSION = PAGE) ON [Index_FG]
GO
