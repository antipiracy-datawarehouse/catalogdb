USE [Catalog]
GO
/****** Object:  Table [Stage].[IspPopularity]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[IspPopularity](
	[CustomerDatabaseId] [smallint] NOT NULL,
	[IspId] [int] NOT NULL,
	[CountryId] [smallint] NOT NULL,
	[PopularityLastRank] [int] NOT NULL,
	[PopularityTopRank] [int] NOT NULL,
	[PopularityTopRankAt] [date] NULL,
	[ReportedTotalCount] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
