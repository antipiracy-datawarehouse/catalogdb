USE [Catalog]
GO
/****** Object:  Table [Stage].[Domain]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[Domain](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[CountryId] [smallint] NULL,
	[IspId] [int] NULL,
	[DisplayDomainId] [int] NULL,
	[WebLanguageId] [smallint] NOT NULL,
	[WebTechnologyId] [tinyint] NOT NULL,
	[WebTypeId] [tinyint] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
