USE [Catalog]
GO
/****** Object:  View [Dim].[v_Titles]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Dim].[v_Titles] 
--WITH SCHEMABINDING  
AS  
SELECT t.Id as TitleId -- GidReferenceId
	,t.LegacyGid as Gid
	,t.TitleTypeId --GR.GidTypeId
	,tt.TitleTypeName --GR.GidTypeName
	,t.TitleName --GR.ContentName
	,t.TitleName2
	,t.TitleNodeId1
	,t.TitleNodeName1
	,t.TitleNodeId2
    ,t.TitleNodeName2
	,t.TitleNodeId3
    ,t.TitleNodeName3
	,t.TitleNodeId4
    ,t.TitleNodeName4
	,t.TitleNodeId5
    ,t.TitleNodeName5
    ,t.TitleNodeId6 
    ,t.TitleNodeName6
    ,t.TitleNodeId7
    ,t.TitleNodeName7 
    ,t.TitleNodeId8
    ,t.TitleNodeName8
    ,t.TitleNodeId9
    ,t.TitleNodeName9
    ,t.TitleNodeId10
    ,t.TitleNodeName10 
    ,isnull(t.LegacyContentItemId1,t.TitleNodeId1) ContentId1
    ,isnull(i.Value, 0) as CompanyId 
	,isNull(i.[Name], 'N/A') as Company
	,isnull(t2.id, t.[TitleNodeId1]) as ContentFranchiseId 
	,isNull(t2.TitleNodeName1, t.[TitleNodeName1]) as ContentFranchise
	,cast(isnull(t2.id, 0) as bit) as IsFranchiseContent
  	,th.id TitleHierarchyId --gt.GidTypeGroupId as ContentTypeId
	,th.Name TitleHierarchy --ContentType  
FROM Title.Title (nolock) t --gr
inner join Title.TitleType (nolock) tt on tt.id=t.TitleTypeId --gt
inner join Title.TitleHierarchy (nolock) th on th.id=tt.TitleHierarchyId -- gtg  
left join [MetaData].[Value] (nolock) v on v.[DefinitionId]=20 and v.TitleNodeId=t.TitleNodeId1
left join [MetaData].[EnumItem] (nolock) i on i.id=v.[EnumValue] and i.[EnumId]=3
left join [MetaData].[Value] (nolock) v2 on v2.[DefinitionId]=26 and v2.TitleNodeId=t.TitleNodeId1
left join Title.Title (nolock) AS t2 on t2.id=v2.IntegerValue
GO
