USE [Catalog]
GO
/****** Object:  Table [Db].[DependentTitle]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Db].[DependentTitle](
	[Id] [int] NOT NULL,
	[CustomerDatabaseId] [smallint] NOT NULL,
	[GidReferenceId] [int] NULL,
	[TitleId] [int] NULL,
	[ValidFromDate] [date] NOT NULL,
	[ValidTillDate] [date] NOT NULL,
	[DependentTitleStatusId] [tinyint] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[CompanyId] [smallint] NULL,
 CONSTRAINT [PK_DependentTitle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_CustomerDatabaseId_GidReferenceId_TitleId] ON [Db].[DependentTitle] 
(
	[CustomerDatabaseId] ASC,
	[GidReferenceId] ASC,
	[TitleId] ASC
)
INCLUDE ( [DependentTitleStatusId]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
GO
