USE [Catalog]
GO
/****** Object:  Table [Domain].[ListDefinition]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Domain].[ListDefinition](
	[Id] [smallint] NOT NULL,
	[ListTypeId] [tinyint] NOT NULL,
	[GidTypeGroupId] [smallint] NULL,
	[CustomerId] [smallint] NULL,
	[ProjectId] [smallint] NULL,
	[Name] [varchar](100) NOT NULL,
	[Purpose] [varchar](256) NULL,
	[Enabled] [bit] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ListDefinition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Name] ON [Domain].[ListDefinition] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
ALTER TABLE [Domain].[ListDefinition] ADD  CONSTRAINT [DF_ListDefinition_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
