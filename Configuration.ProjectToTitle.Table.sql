USE [Catalog]
GO
/****** Object:  Table [Configuration].[ProjectToTitle]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Configuration].[ProjectToTitle](
	[Id] [int] NOT NULL,
	[ProjectId] [smallint] NOT NULL,
	[TitleId] [int] NOT NULL,
	[ValidFrom] [date] NULL,
	[ValidTill] [date] NULL,
	[TierId] [smallint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL,
 CONSTRAINT [pk_Configuration_ProjectToTitle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE NONCLUSTERED INDEX [ix_Configuration_ProjectToTitle_ModifiedAt] ON [Configuration].[ProjectToTitle] 
(
	[ModifiedAt] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
CREATE NONCLUSTERED INDEX [ix_Configuration_TitleId] ON [Configuration].[ProjectToTitle] 
(
	[TitleId] ASC
)
INCLUDE ( [ProjectId]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ux_Configuration_ProjectToTitle_ProjectId_TitleId] ON [Configuration].[ProjectToTitle] 
(
	[ProjectId] ASC,
	[TitleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
