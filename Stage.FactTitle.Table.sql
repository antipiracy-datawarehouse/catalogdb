USE [Catalog]
GO
/****** Object:  Table [Stage].[FactTitle]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[FactTitle](
	[Id] [int] NOT NULL,
	[GidTypeId] [tinyint] NOT NULL,
	[TitleItemId1] [int] NOT NULL,
	[TitleItemId2] [int] NULL,
	[TitleItemId3] [int] NULL,
	[TitleItemName1] [nvarchar](150) NOT NULL,
	[TitleItemName2] [nvarchar](150) NULL,
	[TitleItemName3] [nvarchar](150) NULL,
	[GidReferenceId] [int] NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
