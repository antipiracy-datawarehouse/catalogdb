USE [Catalog]
GO
/****** Object:  Table [Stage].[GidReference]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[GidReference](
	[Id] [int] NOT NULL,
	[Gid] [char](26) NOT NULL,
	[GidTypeId] [tinyint] NOT NULL,
	[ContentItemId1] [int] NOT NULL,
	[ContentItemId2] [int] NULL,
	[ContentItemId3] [int] NULL,
	[ContentItemName1] [nvarchar](256) NOT NULL,
	[ContentItemName2] [nvarchar](256) NULL,
	[ContentItemName3] [nvarchar](256) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedAt] [smalldatetime] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[CompanyId] [smallint] NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
