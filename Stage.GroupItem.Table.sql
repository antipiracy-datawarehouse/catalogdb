USE [Catalog]
GO
/****** Object:  Table [Stage].[GroupItem]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[GroupItem](
	[Id] [int] NOT NULL,
	[GroupDefinitionId] [tinyint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
