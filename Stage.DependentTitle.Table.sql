USE [Catalog]
GO
/****** Object:  Table [Stage].[DependentTitle]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[DependentTitle](
	[Id] [int] NOT NULL,
	[CustomerDatabaseId] [smallint] NOT NULL,
	[GidReferenceId] [int] NULL,
	[TitleId] [int] NULL,
	[ValidFromDate] [date] NOT NULL,
	[ValidTillDate] [date] NOT NULL,
	[DependentTitleStatusId] [tinyint] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[CompanyId] [smallint] NULL
) ON [Stage_FG]
GO
