USE [Catalog]
GO
/****** Object:  Table [Stage].[CustomerDatabase]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[CustomerDatabase](
	[Id] [smallint] NOT NULL,
	[ProjectTypeId] [tinyint] NOT NULL,
	[ContentTypeId] [tinyint] NOT NULL,
	[CustomerId] [smallint] NOT NULL,
	[InsiteProjectId] [int] NULL,
	[Name] [varchar](50) NOT NULL,
	[ServerName] [varchar](30) NOT NULL,
	[HasStaticApproach] [bit] NOT NULL,
	[VerificationModeId] [tinyint] NOT NULL,
	[DataFrom] [date] NULL,
	[DataTill] [date] NULL,
	[Enabled] [bit] NOT NULL,
	[CreatedAt] [smalldatetime] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[UseTitleDependancy] [bit] NOT NULL,
	[UseTitleValidTillFilter] [bit] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
