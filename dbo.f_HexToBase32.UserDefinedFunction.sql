USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[f_HexToBase32]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[f_HexToBase32](@stringHex [nvarchar](max))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[UserDefinedFunctions].[f_HexToBase32]
GO
