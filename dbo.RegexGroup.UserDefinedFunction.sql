USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[RegexGroup]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[RegexGroup](@input [nvarchar](max), @pattern [nvarchar](max), @name [nvarchar](max))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[RegExFunctions].[RegexGroup]
GO
