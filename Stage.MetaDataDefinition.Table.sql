USE [Catalog]
GO
/****** Object:  Table [Stage].[MetaDataDefinition]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[MetaDataDefinition](
	[Id] [int] NOT NULL,
	[MetaDataBasicTypeId] [tinyint] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[DataSourceId] [tinyint] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
