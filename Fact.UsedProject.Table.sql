USE [Catalog]
GO
/****** Object:  Table [Fact].[UsedProject]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Fact].[UsedProject](
	[CustomerDatabaseId] [smallint] NOT NULL,
	[ProjectId] [smallint] NOT NULL,
 CONSTRAINT [PK_UsedProject] PRIMARY KEY CLUSTERED 
(
	[CustomerDatabaseId] ASC,
	[ProjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE NONCLUSTERED INDEX [IX_ProjectId] ON [Fact].[UsedProject] 
(
	[ProjectId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
