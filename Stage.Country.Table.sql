USE [Catalog]
GO
/****** Object:  Table [Stage].[Country]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Country](
	[Id] [smallint] NOT NULL,
	[Code] [varchar](3) NULL,
	[Name] [varchar](50) NOT NULL,
	[DisplayCountryId] [smallint] NULL,
	[Latitude] [decimal](7, 4) NULL,
	[Longitude] [decimal](7, 4) NULL,
	[WorldRegionId] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
