USE [Catalog]
GO
/****** Object:  Table [Title].[Title]    Script Date: 11/24/2020 09:45:08 ******/
SET ARITHABORT ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ARITHABORT ON
GO
CREATE TABLE [Title].[Title](
	[Id] [int] NOT NULL,
	[TitleTypeId] [smallint] NOT NULL,
	[TitleNodeId1] [int] NOT NULL,
	[TitleNodeName1] [nvarchar](256) NOT NULL,
	[TitleNodeId2] [int] NULL,
	[TitleNodeName2] [nvarchar](256) NULL,
	[TitleNodeId3] [int] NULL,
	[TitleNodeName3] [nvarchar](256) NULL,
	[TitleNodeId4] [int] NULL,
	[TitleNodeName4] [nvarchar](256) NULL,
	[TitleNodeId5] [int] NULL,
	[TitleNodeName5] [nvarchar](256) NULL,
	[TitleNodeId6] [int] NULL,
	[TitleNodeName6] [nvarchar](256) NULL,
	[TitleNodeId7] [int] NULL,
	[TitleNodeName7] [nvarchar](256) NULL,
	[TitleNodeId8] [int] NULL,
	[TitleNodeName8] [nvarchar](256) NULL,
	[TitleNodeId9] [int] NULL,
	[TitleNodeName9] [nvarchar](256) NULL,
	[TitleNodeId10] [int] NULL,
	[TitleNodeName10] [nvarchar](256) NULL,
	[TitleName]  AS (CONVERT([nvarchar](500),(((((((([TitleNodeName1]+isnull(' - '+[TitleNodeName2],''))+isnull(' - '+[TitleNodeName3],''))+isnull(' - '+[TitleNodeName4],''))+isnull(' - '+[TitleNodeName5],''))+isnull(' - '+[TitleNodeName6],''))+isnull(' - '+[TitleNodeName7],''))+isnull(' - '+[TitleNodeName8],''))+isnull(' - '+[TitleNodeName9],''))+isnull(' - '+[TitleNodeName10],''),(0))),
	[TitleName2]  AS (CONVERT([nvarchar](500),(((((((([TitleNodeName1]+isnull('|'+[TitleNodeName2],''))+isnull('|'+[TitleNodeName3],''))+isnull('|'+[TitleNodeName4],''))+isnull('|'+[TitleNodeName5],''))+isnull('|'+[TitleNodeName6],''))+isnull('|'+[TitleNodeName7],''))+isnull('|'+[TitleNodeName8],''))+isnull('|'+[TitleNodeName9],''))+isnull('|'+[TitleNodeName10],''),(0))),
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL,
	[LegacyGid] [char](26) NOT NULL,
	[DisplayAsTitleId] [int] NULL,
	[ParentTitleId] [int] NULL,
	[LegacyContentItemId1] [int] NULL,
 CONSTRAINT [pk_Title_Title] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [ix_Title_Title_LegacyGid] ON [Title].[Title] 
(
	[LegacyGid] ASC
)
INCLUDE ( [IsDeleted],
[TitleTypeId]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
CREATE NONCLUSTERED INDEX [ix_Title_Title_ModifiedAt] ON [Title].[Title] 
(
	[ModifiedAt] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF
CREATE NONCLUSTERED INDEX [ix_Title_Title_TitleName] ON [Title].[Title] 
(
	[TitleName] ASC
)
INCLUDE ( [IsDeleted],
[TitleTypeId]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
