USE [Catalog]
GO
/****** Object:  Table [Project].[ProjectContentLink]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Project].[ProjectContentLink](
	[ProjectId] [smallint] NOT NULL,
	[GidReferenceId] [int] NOT NULL,
	[ValidFromDate] [date] NOT NULL,
	[ValidTillDate] [date] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[TierId] [smallint] NOT NULL,
 CONSTRAINT [PK_ProjectContentLink] PRIMARY KEY CLUSTERED 
(
	[ProjectId] ASC,
	[GidReferenceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE NONCLUSTERED INDEX [IX_GidReferenceId] ON [Project].[ProjectContentLink] 
(
	[GidReferenceId] ASC
)
INCLUDE ( [ProjectId]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
ALTER TABLE [Project].[ProjectContentLink] ADD  CONSTRAINT [DF_ProjectContentLink_TierID]  DEFAULT ((-1)) FOR [TierId]
GO
