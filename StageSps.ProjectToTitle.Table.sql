USE [Catalog]
GO
/****** Object:  Table [StageSps].[ProjectToTitle]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [StageSps].[ProjectToTitle](
	[Id] [int] NOT NULL,
	[ProjectId] [smallint] NOT NULL,
	[TitleId] [int] NOT NULL,
	[ValidFrom] [date] NULL,
	[ValidTill] [date] NULL,
	[TierId] [smallint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL
) ON [Stage_FG]
GO
