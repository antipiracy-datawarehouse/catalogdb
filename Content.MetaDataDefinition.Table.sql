USE [Catalog]
GO
/****** Object:  Table [Content].[MetaDataDefinition]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Content].[MetaDataDefinition](
	[Id] [int] NOT NULL,
	[MetaDataBasicTypeId] [tinyint] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[DataSourceId] [tinyint] NOT NULL,
 CONSTRAINT [PK_MetaDataDefinition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
