USE [Catalog]
GO
/****** Object:  Table [Stage].[Isp]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Isp](
	[Id] [int] NOT NULL,
	[Name] [varchar](128) NOT NULL,
	[DisplayIspId] [int] NULL,
	[IsBlackListed] [bit] NOT NULL,
	[IspCategoryId] [smallint] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
