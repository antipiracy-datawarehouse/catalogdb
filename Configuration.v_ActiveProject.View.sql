USE [Catalog]
GO
/****** Object:  View [Configuration].[v_ActiveProject]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [Configuration].[v_ActiveProject]
AS
SELECT DISTINCT
[p].*
FROM
[Configuration].[Project] AS [p]
INNER JOIN [Configuration].[ProjectToTitle] AS [ptt]
ON [ptt].[ProjectId] = [p].[Id]
WHERE
[p].[IsDeleted] = 0
AND CAST(GETUTCDATE() AS DATE) BETWEEN ISNULL([p].[ProjectStart], '2000-01-01') AND ISNULL([p].[ProjectEnd], '2100-01-01')
AND [ptt].[IsDeleted] = 0
AND CAST(GETUTCDATE() AS DATE) BETWEEN ISNULL([ptt].[ValidFrom], '2000-01-01') AND ISNULL([ptt].[ValidTill], '2100-01-01');
GO
