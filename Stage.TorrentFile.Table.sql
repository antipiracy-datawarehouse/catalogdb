USE [Catalog]
GO
/****** Object:  Table [Stage].[TorrentFile]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[TorrentFile](
	[TorrentFileId] [int] NOT NULL,
	[FileId] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
