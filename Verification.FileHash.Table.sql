USE [Catalog]
GO
/****** Object:  Table [Verification].[FileHash]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Verification].[FileHash](
	[Id] [int] NOT NULL,
	[HashText] [varchar](75) NOT NULL,
	[NetworkId] [tinyint] NOT NULL,
	[FileSize] [bigint] NOT NULL,
	[TitleId] [int] NULL,
	[GidReferenceId] [int] NULL,
	[ContentQualityId] [smallint] NOT NULL,
	[FileHashStatusId] [tinyint] NOT NULL,
	[VerifiedAt] [smalldatetime] NULL,
	[IsUploaded4Strem] [bit] NOT NULL,
	[FileFormatId] [smallint] NOT NULL,
	[ContentLanguageId] [smallint] NOT NULL,
	[SubtitleLanguageId] [smallint] NOT NULL,
	[SourceCountryId] [smallint] NOT NULL,
	[FileId] [int] NULL,
	[CreatedAt] [smalldatetime] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_FileHash] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, DATA_COMPRESSION = PAGE) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE NONCLUSTERED INDEX [IX_FileHashStatusId] ON [Verification].[FileHash] 
(
	[FileHashStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [Index_FG]
GO
CREATE NONCLUSTERED INDEX [IX_ModifiedAt] ON [Verification].[FileHash] 
(
	[ModifiedAt] DESC
)
INCLUDE ( [Id],
[NetworkId]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [Index_FG]
ALTER INDEX [IX_ModifiedAt] ON [Verification].[FileHash] DISABLE
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_HashText_NetworkId_FileSize] ON [Verification].[FileHash] 
(
	[HashText] ASC,
	[NetworkId] ASC,
	[FileSize] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [Index_FG]
GO
