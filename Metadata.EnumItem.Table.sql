USE [Catalog]
GO
/****** Object:  Table [Metadata].[EnumItem]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Metadata].[EnumItem](
	[Id] [int] NOT NULL,
	[EnumId] [smallint] NOT NULL,
	[Name] [nvarchar](255) NOT NULL,
	[JsonPropertyName] [nvarchar](255) NOT NULL,
	[DisplayName] [nvarchar](255) NOT NULL,
	[Value] [int] NOT NULL,
	[IsDisabled] [bit] NOT NULL,
 CONSTRAINT [pk_Metadata_EnumItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [ux_Metadata_EnumItem_EnumId_Name] ON [Metadata].[EnumItem] 
(
	[EnumId] ASC,
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
