USE [Catalog]
GO
/****** Object:  Table [Verification].[ShowStatus]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Verification].[ShowStatus](
	[Id] [int] NOT NULL,
	[CustomerDatabaseId] [smallint] NOT NULL,
	[FileHashId] [int] NOT NULL,
	[FirstShowAt] [date] NOT NULL,
	[PopularityLastRank] [int] NOT NULL,
	[PopularityTopRank] [int] NOT NULL,
	[PopularityTopRankAt] [date] NULL,
	[ReportedTotalCount] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_ShowStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 95) ON [Data_FG]
) ON [Data_FG]
GO
CREATE NONCLUSTERED INDEX [IX_FileHashId] ON [Verification].[ShowStatus] 
(
	[FileHashId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [Index_FG]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_CustomerDatabaseId_FileHashId] ON [Verification].[ShowStatus] 
(
	[CustomerDatabaseId] ASC,
	[FileHashId] ASC
)
INCLUDE ( [Id],
[FirstShowAt],
[PopularityLastRank],
[PopularityTopRank],
[ReportedTotalCount]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [Index_FG]
GO
