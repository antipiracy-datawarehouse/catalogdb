USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[f_Base32ToHex]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[f_Base32ToHex](@stringBase32 [nvarchar](max))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[UserDefinedFunctions].[f_Base32ToHex]
GO
