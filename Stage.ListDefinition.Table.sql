USE [Catalog]
GO
/****** Object:  Table [Stage].[ListDefinition]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[ListDefinition](
	[Id] [smallint] NOT NULL,
	[ListTypeId] [tinyint] NOT NULL,
	[GidTypeGroupId] [smallint] NULL,
	[CustomerId] [smallint] NULL,
	[ProjectId] [smallint] NULL,
	[Name] [varchar](100) NOT NULL,
	[Purpose] [varchar](256) NULL,
	[Enabled] [bit] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
