USE [Catalog]
GO
/****** Object:  View [Verification].[v_VerifiedFileHashes2]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [Verification].[v_VerifiedFileHashes2] 
WITH SCHEMABINDING
as
SELECT 
 FH.HashText, FH.NetworkId, FH.FileSize -- unique key 
 ,case FH.NetworkId when 10 then 'BitTorrent'
                    when 20 then 'eDonkey'
                    when 30 then 'Gnutella'
                    when 40 then 'Ares'
                    when 50 then 'DirectConnect' end as ProtocolName
  ,fh.GidReferenceId
  FROM [Verification].[FileHash] as FH
  where fh.FileHashStatusId in (20,22)
GO
