USE [Catalog]
GO
/****** Object:  Table [StageSps].[Tier]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [StageSps].[Tier](
	[Id] [smallint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Description] [varchar](255) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
