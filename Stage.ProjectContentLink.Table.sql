USE [Catalog]
GO
/****** Object:  Table [Stage].[ProjectContentLink]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[ProjectContentLink](
	[ProjectId] [smallint] NOT NULL,
	[GidReferenceId] [int] NOT NULL,
	[ValidFromDate] [date] NOT NULL,
	[ValidTillDate] [date] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[TierId] [smallint] NOT NULL
) ON [Stage_FG]
GO
