USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[f_TextToBinaryHash]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[f_TextToBinaryHash](@ProtocolGid [tinyint], @Hash [nvarchar](max))
RETURNS [varbinary](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[UserDefinedFunctions].[f_TextToBinaryHash]
GO
