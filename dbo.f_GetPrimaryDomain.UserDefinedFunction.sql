USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[f_GetPrimaryDomain]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[f_GetPrimaryDomain](@url [nvarchar](4000))
RETURNS [nvarchar](4000) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[DomainExtractor].[GetPrimaryDomain]
GO
