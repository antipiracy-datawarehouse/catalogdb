USE [Catalog]
GO
/****** Object:  Table [Stage].[ShowStatus]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[ShowStatus](
	[Id] [int] NOT NULL,
	[CustomerDatabaseId] [smallint] NOT NULL,
	[FileHashId] [int] NOT NULL,
	[FirstShowAt] [date] NOT NULL,
	[PopularityLastRank] [int] NOT NULL,
	[PopularityTopRank] [int] NOT NULL,
	[PopularityTopRankAt] [date] NULL,
	[ReportedTotalCount] [int] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
