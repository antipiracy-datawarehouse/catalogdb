USE [Catalog]
GO
/****** Object:  Table [Stage].[CountryPostalCode]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[CountryPostalCode](
	[Id] [int] NOT NULL,
	[CountryId] [smallint] NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Latitude] [decimal](7, 4) NULL,
	[Longitude] [decimal](7, 4) NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
