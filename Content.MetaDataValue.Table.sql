USE [Catalog]
GO
/****** Object:  Table [Content].[MetaDataValue]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Content].[MetaDataValue](
	[ContentItemId] [int] NOT NULL,
	[MetaDataDefinitionId] [int] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_MetaDataValue] PRIMARY KEY CLUSTERED 
(
	[ContentItemId] ASC,
	[MetaDataDefinitionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
