USE [Catalog]
GO
/****** Object:  View [Domain].[v_ExtractionRules]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [Domain].[v_ExtractionRules]
as
SELECT i.[ListDefinitionId]
      ,d.name as [Domain]
  FROM [Domain].[ListItem] i
  inner join Fact.Domain d on d.id=i.[DomainId]
  where i.[ListDefinitionId] in (17,18) and i.[Deleted]=0
GO
