USE [Catalog]
GO
/****** Object:  View [Db].[v_DependentTitles]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [Db].[v_DependentTitles] as
select db.name as CustomerDatabase
      ,ValidFromDate, ValidTillDate, dts.[name] as DependentTitleStatus
      ,gt.[name] as GidType
      ,dt.GidReferenceId as GidReferenceId
      ,dt.TitleId as TitleId
      ,isNull(gr.ContentName, t.TitleName) as ContentName
      ,isNull(gr.ContentItemName1, t.TitleItemName1) as ContentName1
      ,isNull(gr.ContentItemName2, t.TitleItemName2) as ContentName2
      ,isnull(gr.GidTypeId,t.GidTypeId) as GidTypeId, dt.CustomerDatabaseId
      ,TS.PopularityLastRank
      ,TS.PopularityTopRank
      ,TS.ReportedTotalCount
      ,TS.FileHashCount
from [Db].[DependentTitle] as dt
left join [Content].[GidReference] as gr on gr.id=dt.GidReferenceId
left join [Fact].Title as t on t.id=dt.TitleId
inner join Db.CustomerDatabase as db on db.id=dt.CustomerDatabaseId and db.[Enabled]=1 and db.ProjectTypeId in (7,9,10,11) and
        isnull(Db.DataTill,'2000-01-01')>dateadd(month,-1,getutcdate()) and 
       (db.ContentTypeId not in (2,4) or db.Id in (1016,1018,1074,1107) or db.Id>1170)
inner join Content.GidType as gt on gt.id=isnull(gr.GidTypeId,t.GidTypeId)
inner join Db.DependentTitleStatus as dts on dts.id=dt.DependentTitleStatusId
left hash join (
select ss.CustomerDatabaseId, FH.GidReferenceId, FH.TitleId
      ,sum(PopularityLastRank) as PopularityLastRank
      ,sum(PopularityTopRank) as PopularityTopRank
      ,sum(ReportedTotalCount) as ReportedTotalCount
      ,Count(*) as FileHashCount
from Verification.FileHash as FH 
inner join Verification.ShowStatus as ss on ss.FileHashId=FH.id
where FileHashStatusId in (20,22)
group by ss.CustomerDatabaseId, FH.GidReferenceId, FH.TitleId
) as TS on TS.CustomerDatabaseId=dt.CustomerDatabaseId and (TS.GidReferenceId=dt.GidReferenceId or TS.TitleId=dt.TitleId)
GO
