USE [Catalog]
GO
/****** Object:  Table [Content].[GroupDefinition]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Content].[GroupDefinition](
	[Id] [tinyint] NOT NULL,
	[DataSourceId] [tinyint] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_GroupDefinition] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
