USE [Catalog]
GO
/****** Object:  View [Verification].[v_VerifiedFiles]    Script Date: 11/24/2020 09:45:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [Verification].[v_VerifiedFiles] as
select 
  f.id as FileId
  ,F.Sha1Hash, Sha1Hash32, Md4Hash, Md5Hash
  ,FileSize
  ,f.GidReferenceId, f.TitleId   
  ,isnull(gr.GidTypeId, t.GidTypeId) as GidTypeId
,isnull(gr.ContentName,t.TitleName) as ContentName
,case when  f.GidReferenceId is not null then gr.ContentItemName1 else TitleItemName1 end as ContentName1
,case when  f.GidReferenceId is not null then gr.ContentItemName2 else TitleItemName2 end as ContentName2
,case when  f.GidReferenceId is not null then gr.ContentItemName3 else TitleItemName3 end as ContentName3
,null as VerifiedAt
,f.FileFormatId
,f.ContentQualityId
,f.VerificationTypeId               
from Verification.[File] as f 
  left JOIN Content.GidReference GR ON GR.Id=F.GidReferenceId
  left join Fact.Title t on t.id=F.TitleId
  where FileHashStatusId in (20,22)
GO
