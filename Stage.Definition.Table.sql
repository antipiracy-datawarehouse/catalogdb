USE [Catalog]
GO
/****** Object:  Table [Stage].[Definition]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Definition](
	[Id] [smallint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[JsonPropertyName] [varchar](64) NOT NULL,
	[Description] [varchar](256) NOT NULL,
	[DefinitionCategoryId] [smallint] NOT NULL,
	[DefinitionEditorTypeId] [smallint] NOT NULL,
	[ValueTypeId] [tinyint] NOT NULL,
	[KeyBasicTypeId] [smallint] NULL,
	[KeyEnumId] [smallint] NULL,
	[ValueBasicTypeId] [smallint] NOT NULL,
	[ValueEnumId] [smallint] NULL,
	[DerivationTypeId] [tinyint] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
