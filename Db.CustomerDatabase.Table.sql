USE [Catalog]
GO
/****** Object:  Table [Db].[CustomerDatabase]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Db].[CustomerDatabase](
	[Id] [smallint] NOT NULL,
	[ProjectTypeId] [tinyint] NOT NULL,
	[ContentTypeId] [tinyint] NOT NULL,
	[CustomerId] [smallint] NOT NULL,
	[InsiteProjectId] [int] NULL,
	[Name] [varchar](50) NOT NULL,
	[ServerName] [varchar](30) NOT NULL,
	[HasStaticApproach] [bit] NOT NULL,
	[VerificationModeId] [tinyint] NOT NULL,
	[DataFrom] [date] NULL,
	[DataTill] [date] NULL,
	[Enabled] [bit] NOT NULL,
	[CreatedAt] [smalldatetime] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL,
	[Droped] [bit] NOT NULL,
	[UseTitleDependancy] [bit] NOT NULL,
	[UseTitleValidTillFilter] [bit] NOT NULL,
 CONSTRAINT [PK_CustomerDatabase] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Name] ON [Db].[CustomerDatabase] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
ALTER TABLE [Db].[CustomerDatabase] ADD  CONSTRAINT [DF_Droped]  DEFAULT ((0)) FOR [Droped]
GO
