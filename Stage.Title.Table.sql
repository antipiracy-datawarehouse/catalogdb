USE [Catalog]
GO
/****** Object:  Table [Stage].[Title]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[Title](
	[Id] [int] NOT NULL,
	[LegacyGid] [char](26) NOT NULL,
	[TitleTypeId] [smallint] NOT NULL,
	[TitleNodeId1] [int] NOT NULL,
	[TitleNodeName1] [nvarchar](256) NOT NULL,
	[TitleNodeId2] [int] NULL,
	[TitleNodeName2] [nvarchar](256) NULL,
	[TitleNodeId3] [int] NULL,
	[TitleNodeName3] [nvarchar](256) NULL,
	[TitleNodeId4] [int] NULL,
	[TitleNodeName4] [nvarchar](256) NULL,
	[TitleNodeId5] [int] NULL,
	[TitleNodeName5] [nvarchar](256) NULL,
	[TitleNodeId6] [int] NULL,
	[TitleNodeName6] [nvarchar](256) NULL,
	[TitleNodeId7] [int] NULL,
	[TitleNodeName7] [nvarchar](256) NULL,
	[TitleNodeId8] [int] NULL,
	[TitleNodeName8] [nvarchar](256) NULL,
	[TitleNodeId9] [int] NULL,
	[TitleNodeName9] [nvarchar](256) NULL,
	[TitleNodeId10] [int] NULL,
	[TitleNodeName10] [nvarchar](256) NULL,
	[DisplayAsTitleId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL,
	[ParentTitleId] [int] NULL,
	[LegacyContentItemId1] [int] NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
