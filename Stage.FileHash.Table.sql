USE [Catalog]
GO
/****** Object:  Table [Stage].[FileHash]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[FileHash](
	[Id] [int] NOT NULL,
	[HashText] [varchar](75) NOT NULL,
	[NetworkId] [tinyint] NOT NULL,
	[FileSize] [bigint] NOT NULL,
	[TitleId] [int] NULL,
	[GidReferenceId] [int] NULL,
	[ContentQualityId] [smallint] NOT NULL,
	[FileHashStatusId] [tinyint] NOT NULL,
	[VerifiedAt] [smalldatetime] NULL,
	[IsUploaded4Strem] [bit] NOT NULL,
	[FileFormatId] [smallint] NOT NULL,
	[ContentLanguageId] [smallint] NOT NULL,
	[SubtitleLanguageId] [smallint] NOT NULL,
	[SourceCountryId] [smallint] NOT NULL,
	[FileId] [int] NULL,
	[CreatedAt] [smalldatetime] NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
