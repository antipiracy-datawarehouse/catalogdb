USE [Catalog]
GO
/****** Object:  Table [Title].[TitleType]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Title].[TitleType](
	[Id] [smallint] NOT NULL,
	[TitleHierarchyId] [smallint] NULL,
	[TitleHierarchyNodeId1] [smallint] NOT NULL,
	[TitleHierarchyNodeTypeName1] [varchar](64) NOT NULL,
	[TitleHierarchyNodeId2] [smallint] NULL,
	[TitleHierarchyNodeTypeName2] [varchar](64) NULL,
	[TitleHierarchyNodeId3] [smallint] NULL,
	[TitleHierarchyNodeTypeName3] [varchar](64) NULL,
	[TitleHierarchyNodeId4] [smallint] NULL,
	[TitleHierarchyNodeTypeName4] [varchar](64) NULL,
	[TitleHierarchyNodeId5] [smallint] NULL,
	[TitleHierarchyNodeTypeName5] [varchar](64) NULL,
	[TitleHierarchyNodeId6] [smallint] NULL,
	[TitleHierarchyNodeTypeName6] [varchar](64) NULL,
	[TitleHierarchyNodeId7] [smallint] NULL,
	[TitleHierarchyNodeTypeName7] [varchar](64) NULL,
	[TitleHierarchyNodeId8] [smallint] NULL,
	[TitleHierarchyNodeTypeName8] [varchar](64) NULL,
	[TitleHierarchyNodeId9] [smallint] NULL,
	[TitleHierarchyNodeTypeName9] [varchar](64) NULL,
	[TitleHierarchyNodeId10] [smallint] NULL,
	[TitleHierarchyNodeTypeName10] [varchar](64) NULL,
	[TitleTypeName]  AS (CONVERT([nvarchar](200),(((((((([TitleHierarchyNodeTypeName1]+isnull(' - '+[TitleHierarchyNodeTypeName2],''))+isnull(' - '+[TitleHierarchyNodeTypeName3],''))+isnull(' - '+[TitleHierarchyNodeTypeName4],''))+isnull(' - '+[TitleHierarchyNodeTypeName5],''))+isnull(' - '+[TitleHierarchyNodeTypeName6],''))+isnull(' - '+[TitleHierarchyNodeTypeName7],''))+isnull(' - '+[TitleHierarchyNodeTypeName8],''))+isnull(' - '+[TitleHierarchyNodeTypeName9],''))+isnull(' - '+[TitleHierarchyNodeTypeName10],''),(0))),
	[TitleTypeName2]  AS (CONVERT([nvarchar](200),(((((((([TitleHierarchyNodeTypeName1]+isnull('|'+[TitleHierarchyNodeTypeName2],''))+isnull('|'+[TitleHierarchyNodeTypeName3],''))+isnull('|'+[TitleHierarchyNodeTypeName4],''))+isnull('|'+[TitleHierarchyNodeTypeName5],''))+isnull('|'+[TitleHierarchyNodeTypeName6],''))+isnull('|'+[TitleHierarchyNodeTypeName7],''))+isnull('|'+[TitleHierarchyNodeTypeName8],''))+isnull('|'+[TitleHierarchyNodeTypeName9],''))+isnull('|'+[TitleHierarchyNodeTypeName10],''),(0))),
	[ParentTitleTypeId] [smallint] NULL,
	[TitleHierarchyNodeTypeId] [smallint] NOT NULL,
	[TitleHierarchyNodeTypeName] [varchar](64) NOT NULL,
	[Level] [tinyint] NOT NULL,
	[IsLastLevel] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL,
 CONSTRAINT [pk_Title_TitleType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
