USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[GetFileExtension]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[GetFileExtension](@fileName [nvarchar](max))
RETURNS [nvarchar](max) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[UserDefinedFunctions].[GetFileExtension]
GO
