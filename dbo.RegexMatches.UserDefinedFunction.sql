USE [Catalog]
GO
/****** Object:  UserDefinedFunction [dbo].[RegexMatches]    Script Date: 11/24/2020 09:45:11 ******/
CREATE FUNCTION [dbo].[RegexMatches](@input [nvarchar](max), @pattern [nvarchar](max))
RETURNS  TABLE (
	[Index] [int] NULL,
	[Text] [nvarchar](max) NULL
) WITH EXECUTE AS CALLER
AS 
EXTERNAL NAME [SqlClrLibrary].[RegExFunctions].[RegexMatches]
GO
