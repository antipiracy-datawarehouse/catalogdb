USE [Catalog]
GO
/****** Object:  Table [Stage].[OrganizationType]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[OrganizationType](
	[Id] [smallint] NOT NULL,
	[Name] [varchar](30) NOT NULL,
	[ModifiedAt] [smalldatetime] NOT NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
