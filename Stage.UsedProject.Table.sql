USE [Catalog]
GO
/****** Object:  Table [Stage].[UsedProject]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Stage].[UsedProject](
	[CustomerDatabaseId] [smallint] NOT NULL,
	[ProjectId] [smallint] NOT NULL
) ON [Stage_FG]
GO
