USE [Catalog]
GO
/****** Object:  Table [Stage].[GidType]    Script Date: 11/24/2020 09:45:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Stage].[GidType](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Descr] [varchar](256) NULL,
	[GidTypeGroupId] [tinyint] NOT NULL,
	[LevelNo] [tinyint] NOT NULL,
	[IsLastLevel] [bit] NOT NULL,
	[LevelName] [varchar](64) NULL
) ON [Stage_FG]
GO
SET ANSI_PADDING ON
GO
