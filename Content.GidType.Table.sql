USE [Catalog]
GO
/****** Object:  Table [Content].[GidType]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Content].[GidType](
	[Id] [tinyint] NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Descr] [varchar](256) NULL,
	[GidTypeGroupId] [tinyint] NOT NULL,
	[LevelNo] [tinyint] NOT NULL,
	[IsLastLevel] [bit] NOT NULL,
	[LevelName] [varchar](64) NULL,
 CONSTRAINT [PK_Gid_Type] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 70) ON [Data_FG]
) ON [Data_FG]
GO
SET ANSI_PADDING ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [UX_Name] ON [Content].[GidType] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
