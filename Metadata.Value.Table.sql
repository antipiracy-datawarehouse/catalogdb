USE [Catalog]
GO
/****** Object:  Table [Metadata].[Value]    Script Date: 11/24/2020 09:45:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Metadata].[Value](
	[Id] [bigint] NOT NULL,
	[TitleNodeId] [int] NOT NULL,
	[DefinitionId] [smallint] NOT NULL,
	[ValueTypeId] [tinyint] NOT NULL,
	[IntegerKey] [bigint] NULL,
	[EnumKey] [int] NULL,
	[StringKey] [nvarchar](1024) NULL,
	[IntegerValue] [bigint] NULL,
	[EnumValue] [int] NULL,
	[FloatValue] [real] NULL,
	[DateValue] [date] NULL,
	[DateTimeValue] [datetime2](2) NULL,
	[StringValue] [nvarchar](2048) NULL,
	[IsDeleted] [bit] NOT NULL,
	[ModifiedAt] [datetime2](2) NOT NULL,
 CONSTRAINT [pk_Metadata_Value] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Data_FG]
) ON [Data_FG]
GO
CREATE NONCLUSTERED INDEX [ix_Metadata_Value_ModifiedAt] ON [Metadata].[Value] 
(
	[ModifiedAt] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
CREATE NONCLUSTERED INDEX [ix_Metadata_Value_TitleNodeId_DefinitionId] ON [Metadata].[Value] 
(
	[TitleNodeId] ASC,
	[DefinitionId] ASC
)
INCLUDE ( [Id],
[IsDeleted]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [Index_FG]
GO
